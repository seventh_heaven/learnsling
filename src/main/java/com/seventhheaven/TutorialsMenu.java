package com.seventhheaven;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
// SPM: for titlebar bgcolor
import android.graphics.drawable.ColorDrawable;
import android.graphics.Color;
// SPM: for tutorials button
import android.widget.ImageView;
// SPM: for Context Menu
import android.view.Menu;
import android.view.MenuItem;
// SPM: remove this import if you will remove the Toast message below
import android.widget.Toast;
// SPM: for click functionality of image Tutorials BACK button
import android.view.View;
// SPM: for tutorials bar
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
// SPM: FOR TUTORIASL MAIN MENU
import android.app.AlertDialog;
import android.database.Cursor;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.content.Context;

public class TutorialsMenu extends Fragment
{
	ExpandableListAdapter listAdapter;
	ExpandableListView expListView;
	List<String> listDataHeader;
	HashMap<String, List<String>> listDataChild;
	// SPM: FOR TUTORIALS MAIN MENU
	DatabaseHelper db;
	Context ctx;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ctx = getActivity();
		ToolBar tb = new ToolBar(ctx);
		LinearLayout layout = tb.getToolBarLayout(R.drawable.btnbacktutorials24);
		tb.setToolBar("#d6ecff", "#063a66", "TUTORIALS");
		tb.setBackButtonFuntionality(this, new MainMenu());
		// SPM: FOR TUTORIALS MAIN MENU
		try {
			db = new DatabaseHelper(ctx);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		expListView = new ExpandableListView(ctx);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
		expListView.setLayoutParams(params);
		// getData MainMenu and Sub Menu from SQLite DB
		prepareListData();
		listAdapter = new ExpandableListAdapter(ctx, listDataHeader, listDataChild);
		expListView.setAdapter(listAdapter);
		expListView.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				return false;
			}
		});
		expListView.setOnGroupExpandListener(new OnGroupExpandListener() {
			@Override
			public void onGroupExpand(int groupPosition) {
				Toast.makeText(ctx, listDataHeader.get(groupPosition) + " Expanded", Toast.LENGTH_SHORT).show();
			}
		});
		expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {
			@Override
			public void onGroupCollapse(int groupPosition) {
				Toast.makeText(ctx, listDataHeader.get(groupPosition) + " Collapsed", Toast.LENGTH_SHORT).show();
			}
		});
		expListView.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				String mainTopic = listDataHeader.get(groupPosition);
				String subTopic = listDataChild.get(mainTopic).get(childPosition);
				Toast.makeText(ctx, mainTopic + " : " + subTopic, Toast.LENGTH_SHORT).show();
				// if(groupPosition == 0 && childPosition == 0) {
					TutorialsContent.pdfFileName = (groupPosition+1) + "-" + (childPosition+1) + ".pdf";
					TutorialsContent.title = mainTopic; 
					Intent c = new Intent(ctx, TutorialsContent.class);
					startActivity(c);
				// }
				return false;
			}
		});
		layout.addView(expListView);
		return(layout);
	}

	// SPM: FOR TUTORIALS MAIN MENU
	public void showMessage(String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setCancelable(true);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.show();
	}

	// JSG: for exandable menu
	private void prepareListData() {
		listDataHeader = new ArrayList<String>();
		listDataChild = new HashMap<String, List<String>>();
		// SPM: get Main Menu and Sub Menu from SQLite Database
 		Cursor res = db.getTutorialsMainMenu();
		if(res.getCount() == 0) {
			showMessage("Error", "Nothing found");
			return;
		}
		int mainMenuCtr = 0;
		while(res.moveToNext()) {
			int tid = res.getInt(0);
			int num = res.getInt(1);
			Cursor r = db.getTutorialsSubMenu(tid);
			listDataHeader.add(num + " " + res.getString(2));
			List<String> subTopics = new ArrayList<String>();
			while(r.moveToNext()) {
				subTopics.add(num + "-" + r.getString(2) + " " + r.getString(3));
			}
			listDataChild.put(listDataHeader.get(mainMenuCtr++), subTopics);
		}
	}
}
