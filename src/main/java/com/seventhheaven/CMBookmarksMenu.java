package com.seventhheaven;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
// MSM: for titlebar bgcolor
import android.graphics.drawable.ColorDrawable;
import android.graphics.Color;
// MSM: for tutorials button
import android.widget.ImageView;
// MSM: for Context Menu
import android.view.Menu;
import android.view.MenuItem;
// MSM: remove this import if you will remove the Toast message below
import android.widget.Toast;
// MSM: for click functionality of image Tutorials BACK button
import android.view.View;
// MSM: for tutorials bar
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;
// MSM: for Bookmarks Main Menu
import android.app.AlertDialog;
import android.database.Cursor;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.content.Context;

public class CMBookmarksMenu extends Fragment implements OnItemClickListener
{
	//START *** MSM: for Bookmarks Main Menu
	private DatabaseHelper db;
	private ListView listView;
	private List<CMBookmarksRowItem> rowItems;
	private Context ctx;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ctx = getActivity();
		ToolBar tb = new ToolBar(ctx);
		LinearLayout layout = tb.getToolBarLayout(R.drawable.btnbackdark24);
		tb.setToolBar("#d0edf4", "#34495e", "BOOKMARKS");
		tb.setBackButtonFuntionality(this, new MainMenu());
		try {
			db = new DatabaseHelper(ctx);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		rowItems = new ArrayList<CMBookmarksRowItem>();
		Cursor res = db.getBookmarksMenu();
		if(res.getCount() == 0) {
			showMessage("SYSTEM MESSAGE", "No Bookmarks yet!");
		}
		else {
			System.out.println("may nakuhang bookmarks");
			while(res.moveToNext()) {
				int tnum = res.getInt(1);
				int snum = res.getInt(2);
				String bdate = res.getString(3);
				System.out.println("pumasok sa unang while");
				Cursor r = db.getBookmarkTitle(tnum, snum);
				System.out.println("success ang getbookmarktitle");
				while(r.moveToNext()) {
					System.out.println("may nakuhang subtitle");
					String subtitle = r.getString(3);
					String content = r.getString(4);
					CMBookmarksRowItem item = new CMBookmarksRowItem(tnum, snum, bdate, subtitle, content);
					rowItems.add(item);
				}
			}
		}
		System.out.println("natapos ang while loop");
		listView = new ListView(ctx);
		CMBookmarksAdapter adapter = new CMBookmarksAdapter(ctx, rowItems);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
		System.out.println("nadisplay ang bookmarks");
		layout.addView(listView);
		return(layout);
	}

	// MSM: for Bookmarks Main Menu
	public void showMessage(String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setCancelable(true);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.show();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		CMBookmarksRowItem sel = rowItems.get(position);
		String mainTopic = db.getTutorialsMainTopic(sel.getTopicNumber());
		if(mainTopic == null) {
			showMessage("SYSTEM MESSAGE", "No Main Topic Found!");
		}
		TutorialsContent.title = sel.getTopicNumber() + " " + mainTopic;
		TutorialsContent.pdfFileName = sel.getTopicNumber() + "-" + sel.getSubtopicNumber() + ".pdf";
		Intent c = new Intent(ctx, TutorialsContent.class);
		startActivity(c);
	}
}
