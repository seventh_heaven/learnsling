package com.seventhheaven;

import android.os.Bundle;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
// SPM: for Main Menu buttons (Tutorials, Examples, Quizzes)
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.view.Gravity;
// SPM: for Device Screen Orientation
import android.content.res.Configuration;
import android.widget.Toast;
import android.app.Activity;
import android.util.DisplayMetrics;
import android.support.v4.app.FragmentManager;

//MAT: this is the three ImageView before
//MAT: I moved it to this class so the navigation drawer is separated to the Main Menu
public class MainMenu extends Fragment
{
	private Context ctx;
	private LinearLayout layout;
	private LinearLayout.LayoutParams params;
	private int stat = 0; // 0 for portrait, 1 for landscape
	private int screenHeight, screenWidth, btnHeight, btnWidth, mleft, mright, mtop, mbottom;
	private ImageView btntutorials, btnexamples, btnquizzes;
	private FragmentActivity act;
	private FragmentManager manager;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ctx = getActivity();
		act = (FragmentActivity)ctx;
		manager = act.getSupportFragmentManager();
		// SPM: for Main Menu buttons (Tutorials, Examples, Quizzes)
		DisplayMetrics displaymetrics = new DisplayMetrics();
		((Activity) ctx).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		((Activity) ctx).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		// SPM: for Tutorials, Examples and Quizzes button
		layout = new LinearLayout(ctx);
		btntutorials = new ImageView(ctx);
		btnexamples = new ImageView(ctx);
		btnquizzes = new ImageView(ctx);
		checkOrientation();
		if(stat == 0) {
			layoutPortrait();
		}
		else if(stat == 1) {
			layoutLandscape();
		}
		layout.setGravity(Gravity.CENTER);
		params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		layout.setLayoutParams(params);
		LinearLayout.LayoutParams pa = new LinearLayout.LayoutParams(btnWidth, btnHeight);
		btntutorials.setLayoutParams(pa);
		btnquizzes.setLayoutParams(pa);
		LinearLayout.LayoutParams pb = new LinearLayout.LayoutParams(btnWidth, btnHeight);
		pb.setMargins(mleft, mtop, mright, mbottom);
		btnexamples.setLayoutParams(pb);
		// SPM: for buttons image click event
		btnimgClick(btntutorials, new TutorialsMenu());
		btnimgClick(btnexamples, new ExamplesMenu());
		btnimgClick(btnquizzes, new QuizzesMenu());
		layout.addView(btntutorials);
		layout.addView(btnexamples);
		layout.addView(btnquizzes);
		return(layout);
	}

	// SPM: for checking if device is in Portrait or Landscape mode
	private void checkOrientation() {
		int orientation = ctx.getResources().getConfiguration().orientation;
		switch(orientation)
		{
			case Configuration.ORIENTATION_UNDEFINED: stat = 0; break;
			case Configuration.ORIENTATION_LANDSCAPE: stat = 1; break;
			case Configuration.ORIENTATION_PORTRAIT:  stat = 0; break;
			default: stat = 0; break;
		}
	}

	// SPM: set variables to display views in Portrait mode
	private void layoutPortrait() {
		layout.setOrientation(LinearLayout.VERTICAL);
		Toast.makeText(ctx, "PORTRAIT: " + screenHeight + ":" + screenWidth, Toast.LENGTH_SHORT).show();
		btnWidth = screenWidth;
		btnHeight = (screenHeight / 4) + (int)(screenHeight * 0.01);
		mtop = (int)(screenHeight * 0.02);
		mbottom = (int)(screenHeight * 0.02);
		mleft = 0;
		mright = 0;
		btntutorials.setImageResource(R.drawable.btntutorials);
		btnexamples.setImageResource(R.drawable.btnexamples);
		btnquizzes.setImageResource(R.drawable.btnquizzes);
	}

	// SPM: set variables to display views in Landscape mode
	private void layoutLandscape() {
		layout.setOrientation(LinearLayout.HORIZONTAL);
		Toast.makeText(ctx, "LANDSCAPE: " + screenHeight + ":" + screenWidth, Toast.LENGTH_SHORT).show();
		btnWidth = (screenWidth / 3) - (int)(screenWidth * 0.02);
		btnHeight= screenHeight;
		mleft = (int)(screenWidth * 0.01);
		mright = (int)(screenWidth * 0.01);
		mtop = 0;
		mbottom = 0;
		btntutorials.setImageResource(R.drawable.btntutorialslandscape);
		btnexamples.setImageResource(R.drawable.btnexampleslandscape);
		btnquizzes.setImageResource(R.drawable.btnquizzeslandscape);
	}

	// SPM: for buttons img onClick event
	private void btnimgClick(ImageView btnimg, final Fragment f) {
		btnimg.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				UrlHandler.getAppCompatActivity().setFragment(f, "menu");
			}
		});
	}
}
