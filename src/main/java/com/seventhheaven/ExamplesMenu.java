package com.seventhheaven;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
// MSM: for titlebar bgcolor
import android.graphics.drawable.ColorDrawable;
import android.graphics.Color;
// MSM: for tutorials button
import android.widget.ImageView;
// MSM: for Context Menu
import android.view.Menu;
import android.view.MenuItem;
// MSM: remove this import if you will remove the Toast message below
import android.widget.Toast;
// MSM: for click functionality of image Tutorials BACK button
import android.view.View;
// MSM: for tutorials bar
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;
// MSM: for Bookmarks Main Menu
import android.app.AlertDialog;
import android.database.Cursor;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.content.Context;

//MAT: I changed it from Activity to Fragment
public class ExamplesMenu extends Fragment implements OnItemClickListener 
{
	private DatabaseHelper db;
	private ListView listView;
	private List<ExamplesRowItem> rowItems;
	private Context ctx;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ctx = getActivity();
		ToolBar tb = new ToolBar(ctx);
		LinearLayout layout = tb.getToolBarLayout(R.drawable.btnbackexamples24);
		tb.setToolBar("#d3ffd3", "#0f7036", "EXAMPLES");
		tb.setBackButtonFuntionality(this, new MainMenu());
		try {//ccffcc
			db = new DatabaseHelper(ctx);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		rowItems = new ArrayList<ExamplesRowItem>();
		Cursor res = db.getExamplesMainMenu();
		if(res.getCount() == 0) {
			showMessage("SYSTEM MESSAGE", "No Examples yet!");
		}
		else {
			System.out.println("may nakuhang examples");
			while(res.moveToNext()) {
				int id = res.getInt(0);
				int numbering = res.getInt(1);
				String title = res.getString(2);
				String description = res.getString(3);
				String content = res.getString(4);
				System.out.println("success ang getexamplestitle");
				rowItems.add(new ExamplesRowItem(id, numbering, title, description, content));
			}
		}
		System.out.println("natapos ang while loop");
		listView = new ListView(ctx);
		listView.setAdapter(new ExamplesAdapter(ctx, rowItems));
		listView.setOnItemClickListener(this);
		System.out.println("nadisplay ang examples");
		layout.addView(listView);
		return(layout);
	}

	// MSM: for Bookmarks Main Menu
	public void showMessage(String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setCancelable(true);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.show();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		ExamplesRowItem sel = rowItems.get(position);
		ExamplesContent.title = "Sample Program " + sel.getNumbering() + ": " + sel.getTitle();
		ExamplesContent.pdfFileName = sel.getNumbering() + ".pdf";
		Intent c = new Intent(ctx, ExamplesContent.class);
		startActivity(c);
	}
}
