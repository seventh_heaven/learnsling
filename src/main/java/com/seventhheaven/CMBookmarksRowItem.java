package com.seventhheaven;

public class CMBookmarksRowItem {
	private int tnum;
	private int snum;
	private String bdate;
	private String subtitle;
	private String content;
	 
	public CMBookmarksRowItem(int tnum, int snum, String bdate, String subtitle, String content) {
		this.tnum = tnum;
		this.snum = snum;
		this.bdate = bdate;
		this.subtitle = subtitle;
		this.content = content;
	}

	public int getTopicNumber() {
		return tnum;
	}

	public void setTopicNumber(int tnum) {
		this.tnum = tnum;
	}

	public int getSubtopicNumber() {
		return snum;
	}

	public void setSubtopicNumber(int snum) {
		this.snum = snum;
	}

	public String getBookmarkDate() {
		return bdate;
	}

	public void setBookmarkDate(String bdate) {
		this.bdate = bdate;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
