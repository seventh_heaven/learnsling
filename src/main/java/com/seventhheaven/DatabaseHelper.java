package com.seventhheaven;

import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import java.io.File;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.sql.SQLException;
import android.database.sqlite.SQLiteException;

public class DatabaseHelper extends SQLiteOpenHelper
{
	private Context mycontext;
	private String DATABASE_PATH;
	// learnsling.db must be present in app_name\src\main\assets\databases\
	private static String DATABASE_NAME = "learnsling.db";
	private static int DATABASE_VERSION = 1;
	public SQLiteDatabase mydatabase;

	public DatabaseHelper(Context context) throws IOException, SQLException {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.mycontext = context;
		DATABASE_PATH = "data/data/" + mycontext.getApplicationContext().getPackageName()+"/databases/";
		if(mydatabase != null) {
			System.out.println("Database is already open.");
		}
		else {
			onCreate(mydatabase);
		}
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		boolean dbexist = checkdatabase();
		if(dbexist) {
			System.out.println("Database exists. We only need to open it.");
		}
		else {
			System.out.println("Database doesn't exist. We need to create it.");
			try {
				createdatabase();
				System.out.println("Database has been created successfully.");
			}
			catch(Exception e) {
				System.out.println("Failed to create database." + e);
				return;
			}
		}
		try {
			opendatabase();
			System.out.println("Database has been opened.");
		}
		catch(Exception e) {
			System.out.println("Failed to open database." + e);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	public void createdatabase() throws IOException {
		boolean dbexist = checkdatabase();
		System.out.println("Pumasok sa createdatabase()");
		if(dbexist) {
			System.out.println("Database exists.");
		}
		else {
			System.out.println("Pumasok sa else ng createdatabase()");
			this.getReadableDatabase();
			System.out.println("Make database in ...\\assets\\databases\\ folder readable");
			try {
				copydatabase();
			}
			catch(IOException e) {
				throw new Error("Error copying database from assets to android local storage");
			}
		} 
	}

	private boolean checkdatabase() {
		boolean checkdb = false;
		try {
			String myPath = DATABASE_PATH + DATABASE_NAME;
			File dbfile = new File(myPath);
			checkdb = dbfile.exists();
		}
		catch(SQLiteException e) {
			System.out.println("Database doesn't exist");
		}
		return checkdb;
	}

	private void copydatabase() throws IOException {
		System.out.println("Pumasok sa copydatabase()");
		//Open your local db as the input stream
		InputStream myinput = mycontext.getAssets().open("databases/" + DATABASE_NAME);
		System.out.println("na create ang myinput InputStream");
		// Path to the just created empty db
		String outfilename = DATABASE_PATH + DATABASE_NAME;
		System.out.println(DATABASE_PATH.toString());
		// Open the empty db as the output stream
		// OutputStream myoutput = new FileOutputStream("data/data/com.seventhheaven/databases/" + DATABASE_NAME);
		OutputStream myoutput = new FileOutputStream(DATABASE_PATH + DATABASE_NAME);
		System.out.println("na create ang myoutput OutputStream");
		// transfer byte to inputfile to outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myinput.read(buffer))>0) {
			myoutput.write(buffer,0,length);
		}
		//Close the streams
		myoutput.flush();
		myoutput.close();
		myinput.close();
	}

	public void opendatabase() throws SQLException {
		String mypath = DATABASE_PATH + DATABASE_NAME;
		mydatabase = SQLiteDatabase.openDatabase(mypath, null, SQLiteDatabase.OPEN_READWRITE);
	}

	public Cursor getTutorialsMainMenu() {
		Cursor res = mydatabase.rawQuery("SELECT * FROM tbltutorialsmainmenu", null);
		return res;
	}

	public Cursor getTutorialsSubMenu(int tid) {
		Cursor res = mydatabase.rawQuery("SELECT * FROM tbltutorialssubmenu WHERE tid=" + tid, null);
		return res;
	}

	public String getTutorialsMainTopic(int tnum) {
		Cursor res = mydatabase.rawQuery("SELECT * FROM tbltutorialsmainmenu WHERE numbering=" + tnum, null);
		if(res.getCount() == 1) {
			while(res.moveToNext()) {
				return(res.getString(2));
			}
		}
		return null;
	}

	public Cursor getBookmarksMenu() {
		Cursor res = mydatabase.rawQuery("SELECT * FROM tblbookmarks", null);
		return res;
	}

	public Cursor getBookmarkTitle(int tid, int numbering) {
		Cursor res = mydatabase.rawQuery("SELECT * FROM tbltutorialssubmenu WHERE tid=" + tid + " AND numbering=" + numbering,  null);
		return res;
	}

	public Cursor getExamplesMainMenu() {
		Cursor res = mydatabase.rawQuery("SELECT * FROM tblexamplesmainprogram", null);
		return res;
	}

	public Cursor getExamplesTitle(int tid, int numbering) {
		Cursor res = mydatabase.rawQuery("SELECT * FROM tblexamplessubprogram WHERE tid=" + tid + " AND numbering=" + numbering,  null);
		return res;
	}

	public synchronized void close() {
		if(mydatabase != null) {
			mydatabase.close();
		}
		super.close();
	}
}
