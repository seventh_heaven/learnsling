package com.seventhheaven;

import android.os.Bundle;
import android.content.Context;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

//MAT: I changed this class from Activity to Fragment
public class NMDonate extends Fragment
{
	private Context ctx;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ctx = getActivity();
		ToolBar tb = new ToolBar(ctx);
		LinearLayout layout = tb.getToolBarLayout(R.drawable.btnbackdark24);
		tb.setToolBar("#d0edf4", "#34495e", "DONATE");
		tb.setBackButtonFuntionality(this, new MainMenu());
		TextView tv = new TextView(ctx);
		StringBuilder sb = new StringBuilder();
		tv.setText(getDonateContent(sb));
		tv.setTextColor(Color.parseColor("#000000"));
		layout.addView(tv);
		return(layout);
	}

	public String getDonateContent(StringBuilder sb) {
		//MAT: this is the content of Donate Screen
		sb.append("\n\n\tList of accounts:\n\n");
		sb.append("\t\tPaypal: seventhheaveninc@yahoo.com\n\n");
		sb.append("\t\tBDO:\t\tXXX-XXXXX-XXX-XXX\n\n");
		sb.append("\t\t\t\tSeventh Heaven, Inc.\n\n");
		sb.append("\t\tPaymaya:\t\tXXX-XXXXX-XXX-XXX\n\n");
		sb.append("\t\t\t\t0928-521-6232");
		return(sb.toString());
	}
}
