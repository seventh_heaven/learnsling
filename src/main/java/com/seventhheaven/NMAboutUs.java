package com.seventhheaven;

import android.os.Bundle;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.Button;
import android.content.Intent;
import android.net.Uri;
import android.graphics.drawable.ColorDrawable;
import android.graphics.Color;
import android.widget.LinearLayout;

//MAT: This is the work of ICM, I just changed this class from Activity to Fragment
public class NMAboutUs extends Fragment
{
	private Context ctx;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ctx = getActivity();
		ToolBar tb = new ToolBar(ctx);
		LinearLayout layout = tb.getToolBarLayout(R.drawable.btnbackdark24);
		tb.setToolBar("#d0edf4", "#34495e", "ABOUT US");
		tb.setBackButtonFuntionality(this, new MainMenu());
		TextView tv = new TextView(ctx);
		layout.addView(tv);
		tv.setText("Developers");
		Button btn1 = new Button(ctx);
		btn1.setText("Imee Mitra");
		btn1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://ph.linkedin.com/in/imeldamitra"));
				startActivity(intent);
			}
		});
		layout.addView(btn1);
		Button btn2 = new Button(ctx);
		btn2.setText("Mia Malbas");
		btn2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/clarissa-mia-malbas-10b8ba136"));
				startActivity(intent);
			}
		});
		layout.addView(btn2);
		Button btn3 = new Button(ctx);
		btn3.setText("Marlon Tolentino");
		btn3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://ph.linkedin.com/in/marlon-tolentino-28649a138"));
				startActivity(intent);
			}
		});
		layout.addView(btn3);
		Button btn4 = new Button(ctx);
		btn4.setText("Seven Martinez");
		btn4.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/john-vee-m-i-martinez-03973367"));
				startActivity(intent);
			}
		});
		layout.addView(btn4);
		Button btn5 = new Button(ctx);
		btn5.setText("Jayson Gumboc");
		btn5.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/"));
				startActivity(intent);
			}
		});
		layout.addView(btn5);
		return(layout);
	}
}
