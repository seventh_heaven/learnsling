package com.seventhheaven;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.shockwave.pdfium.PdfDocument;
import android.widget.TextView;
import java.util.List;
import java.io.File;
// SPM: for examples button
import android.widget.ImageView;
// SPM: for click functionality of image Examples BACK button
import android.view.View;
// SPM: FOR Alert Message
import android.app.AlertDialog;
// SPM: FOR checking if pdf file exists in assets\ folder
import java.io.IOException;
import java.io.InputStream;
import android.content.res.AssetManager;

public class ExamplesContent extends Activity implements OnPageChangeListener, OnLoadCompleteListener
{
	private static final String TAG = MainActivity.class.getSimpleName();
	public static String pdfFileName; // value of this var will come from ExamplesMenu.java
	public static String title;
	PDFView pdfView;
	Integer pageNumber = 0;
	String pdfPath; // value is tutorials + pdfFileName which inside the assets folder
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.examplescontent);
		TextView tv_header = (TextView)findViewById(R.id.tv_header);
		tv_header.setText(title);
		ImageView btnbackexamples = (ImageView)findViewById(R.id.btnimg_backexamples);
		btnbackexamples.setImageResource(R.drawable.btnbacktutorialsexampleslight24);
		btnbackexamples.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
		pdfView= (PDFView)findViewById(R.id.pdfView);
		System.out.println("Pumasok sa onCreate() ng ExamplesContent");
		displayFromAsset();
	}

	private void displayFromAsset() {
		// check first if assetFileName exists
		AssetManager mg = getResources().getAssets();
		InputStream is = null;
		pdfPath = "examples/" + pdfFileName;
		try {
			is = mg.open(pdfPath);
			System.out.println(pdfPath);
			System.out.println("Pumasok sa displayFromAsset() ng ExamplesContent");
			// pdfFileName must be present inside assets\ folder
			pdfView.fromAsset(pdfPath)
				.defaultPage(pageNumber)
				.enableSwipe(true)
				.swipeHorizontal(false)
				.onPageChange(this)
				.enableAnnotationRendering(true)
				.onLoad(this)
				.scrollHandle(new DefaultScrollHandle(this))
				.load();
			System.out.println("Na load ang pdf.");
			is = null;
		}
		catch (IOException ex) {
			showMessage("Missing PDF File", "Please update this app.");
		}
	}

	@Override
	public void onPageChanged(int page, int pageCount) {
		pageNumber = page;
		setTitle(String.format("%s %s / %s", pdfPath, page + 1, pageCount));
	}

	@Override
	public void loadComplete(int nbPages) {
		PdfDocument.Meta meta = pdfView.getDocumentMeta();
		printBookmarksTree(pdfView.getTableOfContents(), "-");
	}

	public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
		for (PdfDocument.Bookmark b : tree) {
			Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));
			if (b.hasChildren()) {
				printBookmarksTree(b.getChildren(), sep + "-");
			}
		}
	}

	// SPM: FOR ALERT MESSAGE
	public void showMessage(String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(true);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.show();
	}
}
