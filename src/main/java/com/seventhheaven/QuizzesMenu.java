package com.seventhheaven;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
// SPM: for titlebar bgcolor
import android.graphics.drawable.ColorDrawable;
import android.graphics.Color;
// SPM: for tutorials button
import android.widget.ImageView;
// SPM: for Context Menu
import android.view.Menu;
import android.view.MenuItem;
// SPM: remove this import if you will remove the Toast message below
import android.widget.Toast;
// SPM: for click functionality of image Tutorials BACK button
import android.view.View;
// SPM: for tutorials bar
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.content.Context;

public class QuizzesMenu extends Fragment implements OnItemClickListener
{
	public static final String[] titles = new String[] {
		"Quiz 01: Overview of Sling",
		"Quiz 02: Title B",
		"Quiz 03: Title C",
		"Quiz 04: Title D",
		"Quiz 05: Title E"
	};

	public static final String[] descriptions = new String[] {
		"This quiz covers the basics of Sling Programming Language such as Description, Brief History and",
		"Brief description and list of topics or concepts covered",
		"Brief description and list of topics or concepts covered",
		"Brief description and list of topics or concepts covered",
		"Brief description and list of topics or concepts covered"
	};

	public static final Integer[] images = { R.drawable.examplesicon,
			R.drawable.examplesicon, R.drawable.examplesicon, R.drawable.examplesicon, R.drawable.examplesicon };

	private ListView listView;
	private List<ExamplesRowItem> rowItems;
	private Context ctx;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ctx = getActivity();
		ToolBar tb = new ToolBar(ctx);
		LinearLayout layout = tb.getToolBarLayout(R.drawable.btnbackquizzes24);
		tb.setToolBar("#d0edf4", "#34495e", "QUIZZES");
		tb.setBackButtonFuntionality(this, new MainMenu());
		rowItems = new ArrayList<ExamplesRowItem>();
		for (int i = 0; i < titles.length; i++) {
			// ExamplesRowItem item = new ExamplesRowItem(images[i], titles[i], descriptions[i]);
			// rowItems.add(item);
		}
		listView = new ListView(ctx);
		// ExampleAdapter adapter = new ExampleAdapter(ctx, rowItems);
		// listView.setAdapter(adapter);
		// listView.setOnItemClickListener(this);
		layout.addView(listView);
		return(layout);
	}

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Toast toast = Toast.makeText(ctx,
				"Item " + (position + 1) + ": " + rowItems.get(position),
				Toast.LENGTH_SHORT);
		toast.show();
	}
}
