package com.seventhheaven;

import android.app.Activity;
import android.os.Bundle;
// SPM: for titlebar bgcolor
import android.graphics.drawable.ColorDrawable;
import android.graphics.Color;
//MAT: for Buttons' display and functionalities
import android.widget.Button;
import android.widget.Toast;
import android.widget.ShareActionProvider;
import android.content.Intent;
import android.view.View;
import android.net.Uri;

public class NavigationDrawer extends Activity
{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#0614A9")));
		setContentView(R.layout.navigationdrawer);
		//MAT: method call for Navigation Drawer functionality
		getHomeFunctionality();
		getRateUsFunctionality();
		getFeedbackFunctionality();
		getShareFunctionality();
		getDonateFunctionality();
		getPrivacyPolicyFunctionality();
		getTermsOfServiceFunctionality();
		getJoinCommunityFunctionality();
		getAboutUsFunctionality();
	}

	//MAT: this is the method when the user tap "Home" in navigation drawer menu
	public void getHomeFunctionality() {
		Button btnHome = (Button)findViewById(R.id.btn_home);
		btnHome.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Toast.makeText(getApplicationContext(), "You'll always find your way back HOME", Toast.LENGTH_SHORT).show();
			}
		});
	}

	//MAT: this is the method when the user tap "Rate Us" in navigation drawer menu
	public void getRateUsFunctionality() {
		Button btnRateUs = (Button)findViewById(R.id.btn_rate_us);
		btnRateUs.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
				intent.setData(Uri.parse("https://play.google.com/store/apps"));
				startActivity(intent);
			}
		});
	}

	//MAT: this is the method when the user tap "Feedback" in navigation drawer menu
	public void getFeedbackFunctionality() {
		Button btnFeedback = (Button)findViewById(R.id.btn_feedback);
		btnFeedback.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Toast.makeText(getApplicationContext(), "This is the feedback display", Toast.LENGTH_LONG).show();
			}
		});
	}

	//MAT: this is the method when the user tap "Share This App" in navigation drawer menu
	public void getShareFunctionality() {
		Button btnShare = (Button)findViewById(R.id.btn_share_this_app);
		btnShare.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_SUBJECT, "Learn Sling URL");
				intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps");
				startActivity(Intent.createChooser(intent, "Learn Sling URL"));  
			}
		});
	}

	// MAT: this is the method when the user tap Donate in navigation drawer menu
	public void getDonateFunctionality() {
		Button btnDonate = (Button)findViewById(R.id.btn_donate);
		btnDonate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), NMDonate.class);
				startActivity(intent);
			}
		});
	}

	// MAT: this is the method when the user tap "Private Policy" in navigation drawer menu
	public void getPrivacyPolicyFunctionality() {
		Button btnPrivacyPolicy = (Button)findViewById(R.id.btn_privacy_policy);
		btnPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent privacy = new Intent (Intent.ACTION_VIEW);
				privacy.setData(Uri.parse("http://www.imzmitra.com/learn-sling-privacy-policy/"));
				startActivity(privacy);
			}
		});
	}

	// MAT: this is the method when the user tap "Terms of Service" in navigation drawer menu
	public void getTermsOfServiceFunctionality() {
		Button btnTermsOfService = (Button)findViewById(R.id.btn_terms_of_service);
		btnTermsOfService.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent tos = new Intent (Intent.ACTION_VIEW);
				tos.setData(Uri.parse("http://www.imzmitra.com/learn-sling-terms-of-service/"));
				startActivity(tos);
			}
		});
	}

	// MAT: this is the method when the user tap "Join Community" in navigation drawer menu
	public void getJoinCommunityFunctionality() {
		Button btnJoinCommunity = (Button)findViewById(R.id.btn_join_community);
		btnJoinCommunity.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
				intent.setData(Uri.parse("https://www.facebook.com/"));
				startActivity(intent);
			}
		});
	}

	// MAT: this is the method when the user tap "About Us" in navigation drawer menu
	public void getAboutUsFunctionality() {
		Button btnAboutUs = (Button)findViewById(R.id.btn_about_us);
		btnAboutUs.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), NMAboutUs.class);
				startActivity(intent);  
			}
		});
	}
}
