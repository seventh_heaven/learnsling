package com.seventhheaven;

import android.view.View;
import android.content.Context;
import android.widget.LinearLayout;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.Gravity;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.Toast;
import android.app.Activity;

public class ToolBar
{
	private Context ctx;
	private ImageView backButton;
	private LinearLayout toolBar;
	private TextView title;

	public ToolBar(Context ctx) {
		this.ctx = ctx;
	}

	public LinearLayout getToolBarLayout(int buttonId) {
		LinearLayout mainLayout = new LinearLayout(ctx);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		toolBar = new LinearLayout(ctx);
		toolBar.setOrientation(LinearLayout.HORIZONTAL);
		toolBar.setBackgroundColor(Color.parseColor("#d0edf4"));
		toolBar.setGravity(Gravity.CENTER_VERTICAL);
		backButton = new ImageView(ctx);
		backButton.setImageResource(buttonId);
		backButton.setPadding(7, 7, 7, 7);
		toolBar.addView(backButton);
		title = new TextView(ctx);
		title.setText("Main Content");
		title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
		title.setTextColor(Color.parseColor("#34495e"));
		title.setPadding(7, 7, 7, 7);
		toolBar.addView(title);
		mainLayout.addView(toolBar);
		return(mainLayout);
	}

	public void setToolBar(String background, String textColor, String text) {
		toolBar.setBackgroundColor(Color.parseColor(background));
		title.setTextColor(Color.parseColor(textColor));
		title.setText(text);
	}

	public void setBackButtonFuntionality(final Fragment current, final Fragment previous) {
		backButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				current.getActivity().getSupportFragmentManager().beginTransaction().remove(current).commit();
				current.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, previous).commit();
			}
		});
	}
}
