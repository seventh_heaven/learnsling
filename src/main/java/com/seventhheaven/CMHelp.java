package com.seventhheaven;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
// MSM: for titlebar bgcolor
import android.graphics.drawable.ColorDrawable;
import android.graphics.Color;
// MSM: for tutorials button
import android.widget.ImageView;
// MSM: for Context Menu
import android.view.Menu;
import android.view.MenuItem;
// MSM: remove this import if you will remove the Toast message below
import android.widget.Toast;
// MSM: for click functionality of image Tutorials BACK button
import android.view.View;
// MSM: for tutorials bar
import android.widget.TextView;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.content.Context;


public class CMHelp extends Fragment
{
	private Context ctx;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ctx = getActivity();
		ToolBar tb = new ToolBar(ctx);
		LinearLayout layout = tb.getToolBarLayout(R.drawable.btnbackdark24);
		tb.setToolBar("#d0edf4", "#34495e", "HELP");
		tb.setBackButtonFuntionality(this, new MainMenu());
		TextView tv = new TextView(ctx);
		tv.setText(R.string.text_paragraph);
		layout.addView(tv);
		return(layout);
	}
}
