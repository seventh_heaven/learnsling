package com.seventhheaven;

public class ExamplesRowItem {
	private int id;
	private int numbering;
	private String title;
	private String description;
	private String content;
	 
	public ExamplesRowItem(int id, int num, String title, String desc, String content) {
		this.id = id;
		numbering = num;
		this.title = title;
		description = desc;
		this.content = content;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumbering() {
		return numbering;
	}

	public void setNumbering(int num) {
		numbering = num;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String desc) {
		description = desc;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}