package com.seventhheaven;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.os.Handler;
import android.content.Intent;
import android.content.pm.ActivityInfo;

public class MainActivity extends Activity
{
	boolean timer = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		//MAT: for landing screen image
		ImageView iv = (ImageView)findViewById(R.id.screen);
		iv.setImageResource(R.drawable.splashscreen);
		// SPM: this is to disable Landscape mode
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		startScreeningTime();
	}

	//MAT: onStart() override so that the timer will start after onCreate()
	@Override
	public void onStart() {
		super.onStart();
		timer = true;
	}

	//MAT: for Splash Screen Timer and redirecting to LandingScreen
	public void startScreeningTime() {
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			int p = 0;
			public void run() {
				p++;
				System.out.println("p: " + p);
				if(p == 3000) {
					Intent intent = new Intent(getApplicationContext(), LandingScreen.class);
					startActivity(intent);
					finish();
				}
				if(timer == true && p <= 3000) {
					handler.postDelayed(this, 1);
				}
			}
		}, 1);
	}
}
