package com.seventhheaven;

import android.os.Bundle;
import android.content.Intent;
// SPM: for titlebar bgcolor
import android.graphics.drawable.ColorDrawable;
import android.graphics.Color;
// SPM: for Context Menu
import android.view.Menu;
import android.view.MenuItem;
// SPM: remove this import if you will remove the Toast message below
import android.widget.Toast;
// SPM: for click functionality of image buttons Tutorials, Examples, Quizzes
import android.view.View;
// MAT: Changed LandingScreen to AppCompatActivity
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.view.GravityCompat;
import android.support.design.widget.NavigationView;
import android.widget.ShareActionProvider;
import android.net.Uri;

public class LandingScreen extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
	private DrawerLayout drawer;
	private FragmentManager manager;
	private String selected;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.landingscreen);
		UrlHandler.storeAppCompatActivity(this);
		manager = getSupportFragmentManager();
		Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		toolbar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#0614A9")));
		NavigationView nv = (NavigationView)findViewById(R.id.nav_view);
		nv.setNavigationItemSelectedListener(this);
		drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();
		if(savedInstanceState == null) {
			setFragment(new MainMenu(), "Home");
			nv.setCheckedItem(R.id.nav_home);
		}
	}

	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.nav_home:
				int fragments = manager.getBackStackEntryCount();
				for(; fragments > 0; fragments--) {
					manager.popBackStack();
				}
				break;
			case R.id.nav_sign_in:
				Toast.makeText(this,"SIGN IN", Toast.LENGTH_SHORT).show();
				break;
			case R.id.nav_register:
				Toast.makeText(this,"REGISTER", Toast.LENGTH_SHORT).show();
				break;
			case R.id.nav_faqs:
				Toast.makeText(this,"FAQs", Toast.LENGTH_SHORT).show();
				break;
			case R.id.nav_more_tutorials:
				Toast.makeText(this,"MORE TUTORIALS", Toast.LENGTH_SHORT).show();
				break;
			case R.id.nav_feedback:
				Toast.makeText(this,"FEEDBACK", Toast.LENGTH_SHORT).show();
				break;
			case R.id.nav_rate_us:
				UrlHandler.setUrl("https://play.google.com/store/apps");
				setFragment(new WebViewer(), "Web");
				break;
			case R.id.nav_share_this_app:
				Intent share = new Intent(Intent.ACTION_SEND);
				share.setType("text/plain");
				share.putExtra(Intent.EXTRA_SUBJECT, "Learn Sling URL");
				share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps");
				startActivity(Intent.createChooser(share, "Learn Sling URL"));  
				break;
			case R.id.nav_donate:
				setFragment(new NMDonate(), "Donate");
				break;
			case R.id.nav_privacy_policy:
				UrlHandler.setUrl("http://www.imzmitra.com/learn-sling-privacy-policy/");
				setFragment(new WebViewer(), "Web");
				break;
			case R.id.nav_terms_of_service:
				UrlHandler.setUrl("http://www.imzmitra.com/learn-sling-terms-of-service/");
				setFragment(new WebViewer(), "Web");
				break;
			case R.id.nav_join_community:
				UrlHandler.setUrl("https://www.facebook.com/");
				setFragment(new WebViewer(), "Web");
				break;
			case R.id.nav_about_us:
				setFragment(new NMAboutUs(), "About");
				break;
		}
		drawer.closeDrawer(GravityCompat.START);
		return(true);
	}

	@Override
	public void onBackPressed() {
		Fragment fragment = manager.findFragmentById(R.id.fragment_container);
		if(drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		}
		else {
			if("Home".equals(selected)) {
				super.onBackPressed();
			}
			else {
				setFragment(new MainMenu(), "Home");
			}
		}
	}

	// SPM: for Context Menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("Bookmarks");
		menu.add("Help");
		menu.add("Close");
		return(true);
	}

	// SPM: for Context Menu toast only v1
	// MSM: for Context Menu functionalities v2
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if("Bookmarks".equals(item.getTitle())){
			setFragment(new CMBookmarksMenu(), "Bookmarks");
		}
		else if("Help".equals(item.getTitle())) {
			setFragment(new CMHelp(), "Help");
		}
		else if("Close".equals(item.getTitle())) {
			finish();
		}
		return(true);
	}

	public void setFragment(Fragment fragment, String name) {
		manager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
		selected = name;
	}
}
