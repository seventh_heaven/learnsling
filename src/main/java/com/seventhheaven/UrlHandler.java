package com.seventhheaven;

public class UrlHandler
{
	private static String url = "";
	private static LandingScreen ls = null;

	public static void setUrl(String u) {
		url = u;
	}

	public static String getUrl() {
		return(url);
	}

	public static void storeAppCompatActivity(LandingScreen l) {
		ls = l;
	}

	public static LandingScreen getAppCompatActivity() {
		return(ls);
	}
}
