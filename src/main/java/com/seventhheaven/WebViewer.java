package com.seventhheaven;

import android.os.Bundle;
import android.content.Context;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

//MAT: I made this class so we do not use intent in redirecting url
//MAT: instead we use WebView so the action bar and toolbar still display
public class WebViewer extends Fragment
{
	private Context ctx;
	private WebView wv;
	private String url;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ctx = getActivity();
		ToolBar tb = new ToolBar(ctx);
		LinearLayout layout = tb.getToolBarLayout(R.drawable.btnbackdark24);
		tb.setToolBar("#d0edf4", "#34495e", "WEB");
		tb.setBackButtonFuntionality(this, new MainMenu());
		wv = new WebView(ctx);
		wv.loadUrl(UrlHandler.getUrl());
		layout.addView(wv);
		return(layout);
	}
}
