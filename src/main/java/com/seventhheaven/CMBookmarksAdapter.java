package com.seventhheaven;
 
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
 
public class CMBookmarksAdapter extends BaseAdapter {
	Context context;
	List<CMBookmarksRowItem> rowItems;
	
	public CMBookmarksAdapter(Context context, List<CMBookmarksRowItem> items) {
		this.context = context;
		this.rowItems = items;
	}
	
	/*private view holder class*/
	private class ViewHolder {
		TextView txtTopic;
		TextView txtTimestamp;
		ImageView imageView;
	}
 
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		LayoutInflater mInflater = (LayoutInflater)
			context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.bookmarks_listitem, null);
			holder = new ViewHolder();
			holder.txtTopic = (TextView)convertView.findViewById(R.id.topic);
			holder.txtTimestamp = (TextView)convertView.findViewById(R.id.timestamp);
			holder.imageView = (ImageView)convertView.findViewById(R.id.icon);
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder)convertView.getTag();
		}
		CMBookmarksRowItem rowItem = (CMBookmarksRowItem)getItem(position);
		int tnum = rowItem.getTopicNumber();
		int snum = rowItem.getSubtopicNumber();
		String subtitle = rowItem.getSubtitle();
		holder.txtTopic.setText(tnum + "-" + snum + " " + subtitle);
		holder.txtTimestamp.setText(rowItem.getBookmarkDate());
		holder.imageView.setImageResource(R.drawable.bookmarksicon);
		return convertView;
	}

	@Override
	public int getCount() {
		return rowItems.size();
	}
 
	@Override
	public Object getItem(int position) {
		return rowItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return rowItems.indexOf(getItem(position));
	}
}
